import newton
from numpy import *

def solveCubic(a, tol=1e-14, maxIterations=15):
    """
    Takes input parameters coeffecients of the cubic equation, tol,
    maxIterations.
    
    Returns output parameters roots, success flag, and error conditions.
    """
    roots = []
    successFlag = True
    errorMsg = ''

    # do not process non-cubic functions
    if a[3] == 0: return (roots, False, 'Coefficient of x^3 is zero.  Not solving as a cubic.')

    # normalize
    a = a/a[3]

    # shift
    shift = a[2]/3
    b = (a[2] ** 2)/3 - a[1]
    c = (a[2]/3) * (a[1] - ((2.0/9) * (a[2] ** 2))) - a[0]

    # define function and derivatives for Newton's Method
    fx = lambda x: a[3] * x**3 + a[2] * x**2 + a[1] * x + a[0]
    dfx = lambda x: 3 * a[3] * x**2 + 2 * a[2] * x + a[1]

    # find initial values and run Newton's Method
    if b > 0:
        if abs(c) == 2 * pow(b/3, 1.5):
            # double root
            roots.append([-sqrt(b/3) * sign(c) - shift, True, ''])

            # use Newton's Method to find the other root
            x0 = c/(2*b) + sign(c) * sqrt(b) - shift
            roots.append(newton.runNewtonsMethod(fx, dfx, x0, maxIterations, tol))
        elif abs(c) < 2 * pow(b/3, 1.5):
            # use Newton's Method to find three real roots
            x0 = -c/b - shift
            roots.append(newton.runNewtonsMethod(fx, dfx, x0, maxIterations, tol))

            x0 = c/(2*b) - sign(c) * sqrt(b) - shift
            roots.append(newton.runNewtonsMethod(fx, dfx, x0, maxIterations, tol))
            x0 = c/(2*b) + sign(c) * sqrt(b) - shift
            roots.append(newton.runNewtonsMethod(fx, dfx, x0, maxIterations, tol))
        else:
            if c ** 2 > abs(b ** 3):
                # Use Newton's Method to find one root
                x0 = c ** 1/3 - shift
                roots.append(newton.runNewtonsMethod(fx, dfx, x0, maxIterations, tol))
            else:
                # Use Newton's Method to find one root
                x0 = c/(2*b) + sign(c) * sqrt(b) - shift
                roots.append(newton.runNewtonsMethod(fx, dfx, x0, maxIterations, tol))
    else:
        if c ** 2 > abs(b ** 3):
            # Use Newton's Method to find one root
            x0 = c ** 1/3 - shift
            roots.append(newton.runNewtonsMethod(fx, dfx, x0, maxIterations, tol))
        else:
            # Use Newton's Method to find one root
            x0 = -c/(2*b) - shift
            roots.append(newton.runNewtonsMethod(fx, dfx, x0, maxIterations, tol))

    # Report any errors found by Newton's Method
    for root in roots:
        if root[1] == False:
            successFlag = False
            errorMsg = root[2]

    return (roots, successFlag, errorMsg)
