FILES = 1 2 3 4 5

all:
	$(foreach f,$(FILES),python pp1.py < test/pp1TestFile$(f).txt > output_pp1TF$(f).txt;)

test: all
	python testnewton.py
	python testcubic.py

outputtest: all
	$(foreach f,$(FILES),colordiff -b output_pp1TF$(f).txt test/output_pp1TF$(f).txt;)


clean:
	rm -rf *.pyc
	rm -f output_pp1TF*.txt
