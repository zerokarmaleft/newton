#!/usr/bin/env python

import newton
import unittest

from numpy import *
from numpy.testing import *

class testNewton(unittest.TestCase):
    """
    A test class for the Newton module.
    """

    def setUp(self):
        self.fx = lambda x: 2 * cosh(x / 4) - x
        self.dfx = lambda x: 0.5 * sinh(x / 4) - 1
        self.maxIterations = 15
        self.eps = finfo(double).eps

    def testRoots(self):
        x0 = 2.0
        r0 = newton.runNewtonsMethod(self.fx, self.dfx, x0, self.maxIterations, self.eps)
        assert_approx_equal(r0[0], 2.35755, significant=5)

        x0 = 8.0
        r1 = newton.runNewtonsMethod(self.fx, self.dfx, x0, self.maxIterations, self.eps)
        assert_approx_equal(r1[0], 8.5072, significant=5)

def suite():
	suite = unittest.TestSuite()
	suite.addTest(unittest.makeSuite(testNewton))
	return suite

if __name__ == "__main__":
	unittest.main()
