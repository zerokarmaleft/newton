#!/usr/bin/env python

from numpy import *
import newton
import cubic
import sys

if __name__ == "__main__":
    # default tolerance and max iterations for Newton's Method
    tol = 1e-14
    maxIterations = 15

    for line in sys.stdin:
        # load coefficients, tolerance, and max iterations from input file
        args = line.split()
        if len(args) == 4:
            a = array(double(args))
        elif len(args) == 5:
            a = array(double(args[0:4]))
            tol = double(args[4])
        elif len(args) == 6:
            a = array(double(args[0:4]))
            tol = double(args[4])
            maxIterations = int(args[5])

        # define f(x) as a cubic function
        f = lambda x: (a[3] * x**3) + (a[2] * x**2) + (a[1] * x) + a[0]
        roots = cubic.solveCubic(a, tol, maxIterations)

        # display f(x) with given coefficients
        print '{0[3]}x^3 + {0[2]}x^2 + {0[1]}x +{0[0]} = 0'.format(a)

        # display roots of f(x)
        if roots[0] == 0:
            print '     no roots found.'
        else:
            i = 0
            for root in roots[0]:
                print '     root[{}] at {: 1.13e} with f(root[{}]) = {: 1.13e}'.format(i, root[0], i, f(root[0]))
                i += 1

        # display any errors
        if roots[2] == '':
            print '     with no error flag'
        else:
            print '     with error flag and message :', roots[2]
