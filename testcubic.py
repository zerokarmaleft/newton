#!/usr/bin/env python

import cubic
import unittest

from numpy import *
from numpy.testing import *

class testCubic(unittest.TestCase):
    """
    A test class for the Cubic module.
    """

    def testpp1TestFile1F1(self):
        a = array([4.0, -2.0, -4.0, 2.0])
        roots = cubic.solveCubic(a)
        actual = [r[0] for r in roots[0]]
        self.assertEqual(len(actual), 3)
        assert_approx_equal(actual[0], 1.0)
        assert_approx_equal(actual[1], 2.0)
        assert_approx_equal(actual[2], -1.0)

    def testpp1TestFile1F2(self):
        a = array([-2.0, 1.0, -2.0, 1.0, .0])
        roots = cubic.solveCubic(a)
        actual = [r[0] for r in roots[0]]
        self.assertEqual(len(actual), 1)
        assert_approx_equal(actual[0], 2.0)

    def testpp1TestFile2F3(self):
        a = array([9.0, 2.0, -2.0, 0.0])
        roots = cubic.solveCubic(a)
        self.assertEqual(len(roots[0]), 0)
        self.assertFalse(roots[1])

    def testpp1TestFile2F4(self):
        a = array([1.0, 2.0, 3.0, 4.0])
        roots = cubic.solveCubic(a)
        actual = [r[0] for r in roots[0]]
        self.assertEqual(len(actual), 1)
        assert_approx_equal(actual[0], -0.60583, significant=5)

    def testpp1TestFile2F5(self):
        a = array([5.0, 2.0, 3.0, 4.0])
        roots = cubic.solveCubic(a)
        actual = [r[0] for r in roots[0]]
        self.assertEqual(len(actual), 1)
        assert_approx_equal(actual[0], -1.20066, significant=5)

    def testpp1TestFile2F6(self):
        a = array([-1.0, 1.0, -4.0, 1.0])
        roots = cubic.solveCubic(a)
        actual = [r[0] for r in roots[0]]
        self.assertEqual(len(actual), 1)
        assert_approx_equal(actual[0], 3.8063)

    def testpp1TestFile2F7(self):
        a = array([-2.0, 0.0, 0.0, 1.0])
        roots = cubic.solveCubic(a)
        actual = [r[0] for r in roots[0]]
        self.assertEqual(len(actual), 1)
        assert_approx_equal(actual[0], 1.25992, significant=5)

    def testpp1TestFile2F8(self):
        a = array([0.0, 0.0, 27.0, 1.0])
        roots = cubic.solveCubic(a)
        actual = [r[0] for r in roots[0]]
        self.assertEqual(len(actual), 2)
        assert_approx_equal(actual[0], 0.0)
        assert_approx_equal(actual[1], -27.0)

    def testpp1TestFile3F1(self):
        a = array([1.0, 4.0, 5.0, 2.0])
        roots = cubic.solveCubic(a)
        actual = [r[0] for r in roots[0]]
        self.assertEqual(len(actual), 1)
        assert_approx_equal(actual[0], -0.5, significant=5)
        """
        self.assertEqual(len(actual), 2)
        assert_approx_equal(actual[0], -1.0, significant=5)
        assert_approx_equal(actual[1], -0.5, significant=5)
        """

    def testpp1TestFile3F2(self):
        a = array([-1.0, -4.0, 5.0, 2.0])
        roots = cubic.solveCubic(a)
        actual = [r[0] for r in roots[0]]
        self.assertEqual(len(actual), 3)
        assert_approx_equal(actual[0], -0.202773, significant=5)
        assert_approx_equal(actual[1], 0.796927, significant=5)
        assert_approx_equal(actual[2], -3.09415, significant=5)

    def testpp1TestFile3F3(self):
        a = array([-28.0, 4.0, 5.0, 2.0])
        roots = cubic.solveCubic(a)
        actual = [r[0] for r in roots[0]]
        self.assertEqual(len(actual), 1)
        assert_approx_equal(actual[0], 1.61707, significant=5)

    def testpp1TestFile3F4(self):
        a = array([-1.0, 4.0, 5.0, 2.0])
        roots = cubic.solveCubic(a)
        actual = [r[0] for r in roots[0]]
        self.assertEqual(len(actual), 1)
        assert_approx_equal(actual[0], 0.197429, significant=5)
        """
        self.assertEqual(len(actual), 3)
        assert_approx_equal(actual[0], 0.197429, significant=5)
        assert_approx_equal(actual[1], 0.197429, significant=5)
        assert_approx_equal(actual[2], 0.197429, significant=5)
        """

    def testpp1TestFile3F5(self):
        a = array([35.0, 4.0, 5.0, 2.0])
        roots = cubic.solveCubic(a)
        actual = [r[0] for r in roots[0]]
        self.assertEqual(len(actual), 1)
        assert_approx_equal(actual[0], -3.41495, significant=5)

    def testpp1TestFile3F6(self):
        a = array([2.0, 4.0, 5.0, 2.0])
        roots = cubic.solveCubic(a)
        actual = [r[0] for r in roots[0]]
        self.assertEqual(len(actual), 1)
        assert_approx_equal(actual[0], -1.6573, significant=5)

    def testpp1TestFile3F7(self):
        a = array([1.0, 6.0, -4.0, -24.0])
        roots = cubic.solveCubic(a)
        actual = [r[0] for r in roots[0]]
        self.assertEqual(len(actual), 3)
        assert_approx_equal(actual[0], -0.166667, significant=5)
        assert_approx_equal(actual[1], -0.5, significant=5)
        assert_approx_equal(actual[2], 0.5, significant=5)

    def testpp1TestFile4F1(self):
        a = array([1.0, -3.0, 2.0, 1.0])
        roots = cubic.solveCubic(a)
        actual = [r[0] for r in roots[0]]
        self.assertEqual(len(actual), 1)
        assert_approx_equal(actual[0], -3.0796, significant=5)

    def testpp1TestFile4F2(self):
        a = array([-13.0, 0.0, 0.0, 1.0])
        roots = cubic.solveCubic(a)
        actual = [r[0] for r in roots[0]]
        self.assertEqual(len(actual), 1)
        assert_approx_equal(actual[0], 2.35133, significant=5)

    def testpp1TestFile4F3(self):
        a = array([1.0, 0.0, 0.0, -13.0])
        roots = cubic.solveCubic(a)
        actual = [r[0] for r in roots[0]]
        self.assertEqual(len(actual), 1)
        assert_approx_equal(actual[0], 0.42529, significant=5)

    def testpp1TestFile4F4(self):
        a = array([2.0, 12.0, 22.0, 12.0])
        roots = cubic.solveCubic(a)
        actual = [r[0] for r in roots[0]]
        self.assertEqual(len(actual), 3)
        assert_approx_equal(actual[0], -0.5)
        assert_approx_equal(actual[1], -0.333333, significant=5)
        assert_approx_equal(actual[2], -1.0)

    def testpp1TestFile4F5(self):
        a = array([1.0, 6.0, 11.0, 6.0])
        roots = cubic.solveCubic(a)
        actual = [r[0] for r in roots[0]]
        self.assertEqual(len(actual), 3)
        assert_approx_equal(actual[0], -0.5)
        assert_approx_equal(actual[1], -0.333333, significant=5)
        assert_approx_equal(actual[2], -1.0)

    def testpp1TestFile4F6(self):
        a = array([1.0, 0.0, 0.0, 8.0])
        roots = cubic.solveCubic(a)
        actual = [r[0] for r in roots[0]]
        self.assertEqual(len(actual), 1)
        assert_approx_equal(actual[0], -0.5)

    def testpp1TestFile4F7(self):
        a = array([1.0, -3.0, 2.0, 1.0])
        roots = cubic.solveCubic(a, tol=1e-2)
        actual = [r[0] for r in roots[0]]
        self.assertEqual(len(actual), 1)
        assert_approx_equal(actual[0], -3.0796, significant=5)

    def testpp1TestFile4F8(self):
        a = array([1.0, -3.0, 2.0, 1.0])
        roots = cubic.solveCubic(a, tol=1e-10)
        actual = [r[0] for r in roots[0]]
        self.assertEqual(len(actual), 1)
        assert_approx_equal(actual[0], -3.0796, significant=5)

    def testpp1TestFile4F9_13(self):
        a = array([1.0, -3.0, 2.0, 1.0])

        for i in range(1, 6):
            roots = cubic.solveCubic(a, tol=1e-13, maxIterations=i)
            actual = [r[0] for r in roots[0]]
            self.assertEqual(len(actual), 1)
            assert_approx_equal(actual[0], -3.0796, significant=2)

    def testpp1TestFile4F14(self):
        a = array([0.0, 9.0, 2.0, -10.0])
        roots = cubic.solveCubic(a)
        actual = [r[0] for r in roots[0]]
        self.assertEqual(len(actual), 3)
        assert_approx_equal(actual[0], 0.0)
        assert_approx_equal(actual[1], -0.853939, significant=5)
        assert_approx_equal(actual[2], 1.05394, significant=5)

    def testpp1TestFile5F1(self):
        a = array([-1.0, -4.0, 5.0, 2.0])
        roots = cubic.solveCubic(a)
        actual = [r[0] for r in roots[0]]
        self.assertEqual(len(actual), 3)
        assert_approx_equal(actual[0], -0.202773, significant=5)
        assert_approx_equal(actual[1], 0.796927, significant=5)
        assert_approx_equal(actual[2], -3.09415, significant=5)

    def testpp1TestFile5F2(self):
        a = array([-1.0, -4.0, -5.0, 2.0])
        roots = cubic.solveCubic(a)
        actual = [r[0] for r in roots[0]]
        self.assertEqual(len(actual), 1)
        assert_approx_equal(actual[0], 3.17868, significant=5)

    def testpp1TestFile5F3(self):
        a = array([-1.0, -4.0, -5.0, -2.0])
        roots = cubic.solveCubic(a)
        actual = [r[0] for r in roots[0]]
        self.assertEqual(len(actual), 1)
        assert_approx_equal(actual[0], -0.5, significant=5)
        """
        self.assertEqual(len(actual), 2)
        assert_approx_equal(actual[0], -1.0)
        assert_approx_equal(actual[1], -0.5)
        """

    def testpp1TestFile5F4(self):
        a = array([1.0, -4.0, 5.0, -2.0])
        roots = cubic.solveCubic(a)
        actual = [r[0] for r in roots[0]]
        self.assertEqual(len(actual), 1)
        assert_approx_equal(actual[0], 0.5, significant=5)
        """
        self.assertEqual(len(actual), 2)
        assert_approx_equal(actual[0], 0.5)
        assert_approx_equal(actual[1], 1.0)
        """

    def testpp1TestFile5F5(self):
        a = array([-1.0, 4.0, -5.0, 2.0])
        roots = cubic.solveCubic(a)
        actual = [r[0] for r in roots[0]]
        self.assertEqual(len(actual), 1)
        assert_approx_equal(actual[0], 0.5, significant=5)
        """
        self.assertEqual(len(actual), 2)
        assert_approx_equal(actual[0], 0.5)
        assert_approx_equal(actual[1], 1.0)
        """

    def testpp1TestFile5F6(self):
        a = array([1.0, 6.0, -4.0, -24.0])
        roots = cubic.solveCubic(a)
        actual = [r[0] for r in roots[0]]
        self.assertEqual(len(actual), 3)
        assert_approx_equal(actual[0], -0.166667, significant=5)
        assert_approx_equal(actual[1], -0.5)
        assert_approx_equal(actual[2], 0.5)

def suite():
    suite = unittest.TestSuite()
    suite.addTest(unittest.makeSuite(testCubic))
    return suite

if __name__ == "__main__":
	unittest.main()
