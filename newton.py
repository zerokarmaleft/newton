"""
Newton's Method

Based on Example 3.7, page 51 of Ascher's book
"""

from numpy import *

def runNewtonsMethod(f, df, initialValue, maxIterations, eps):
    """
    Takes input parameters f, df, initialValue, maxIterations, eps.

    Returns output values of an approximate root, success flag, and
    error condition.
    """
    x_k = initialValue

    for i in range(maxIterations):
        # next approximation
        try:
            x_next_k = x_k - (f(x_k) / df(x_k))
        except ZeroDivisionError: return[x_k, False, 'ZeroDivisionError']

        # root is within tolerance
        if abs(x_next_k - x_k) <= (1 + abs(x_next_k)) * eps: return [x_next_k, True, '']

        # tighten with another iteration
        x_k = x_next_k

    return [x_next_k, False, 'Reached maximum number of iterations.']

        
